from django.urls import path

from . import views

app_name = 'pageGenerator'

urlpatterns = [
    path(
        'new/',
        views.EventCreateView.as_view(),
        name='new-event'
    ),
    path(
        '<int:pk>/update/',
        views.EventUpdateView.as_view(),
        name='event-update'
    ),
    path(
        'list/',
        views.EventListView.as_view(),
        name='event-list'
    ),
    path(
        '<int:pk>/detail/',
        views.EventDetailView.as_view(),
        name='event-detail'
    ),
    path(
        'event/<str:token>/speaker-form/',
        views.SpeakerCreateView.as_view(),
        name='speaker-form'
    ),
    path(
        'speaker/<int:pk>/update/',
        views.SpeakerUpdateView.as_view(),
        name='speaker-update'
    ),
    path(
        '<int:pk>/<str:slug>/html/',
        views.HTMLPageView.as_view(),
        name='html-page'
    ),
    path(
        'fetch_image/<int:pk>/<int:img>/',
        views.fetch_image
    ),
]
