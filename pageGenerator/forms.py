from django import forms
from django.conf import settings

from datetime import datetime
from secrets import token_urlsafe
from PIL import Image

from . import models, utils
from accounts import models as _models

class EventForm(forms.ModelForm):

    event_name = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        )
    )

    venue = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        )
    )

    start_date = forms.DateField(
        required=True,
        widget=forms.DateInput(
            attrs={
                'class':'form-control mydatepicker'
            },
            format='%d/%m/%Y'
        ),
        input_formats=('%d/%m/%Y', )
    )

    end_date = forms.DateField(
        required=True,
        widget=forms.DateInput(
            attrs={
                'class':'form-control mydatepicker'
            },
            format='%d/%m/%Y'
        ),
        input_formats=('%d/%m/%Y', )
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        event = kwargs.get('instance', None)
        super().__init__(*args, **kwargs)

        extra_fields = _models.EventExtraField.objects.filter(
            company=self.request.user.company
        )

        for field in extra_fields:
            field_name = field.name

            response = None
            # Fill with initial values if form is getting updated
            if event != None:
                response = models.EventExtraFieldResponse.objects.filter(
                    event=event,
                    question=field
                )
                if response:
                    response = response.first().response

            if field.type=='4':
                choice_raw_list = [str.strip() for str in field.dropdown_choices.split(',')]
                CHOICE_LIST = []
                for i, choice in enumerate(choice_raw_list):
                    CHOICE_LIST.append((choice, choice))

                self.fields[field_name] = forms.ChoiceField(
                    choices=CHOICE_LIST,
                    widget=forms.Select(
                        attrs={
                            'class':'form-control custom-select'
                        }
                    )
                )
                if response:
                    self.fields[field_name].initial = response
            elif field.type=='3':
                self.fields[field_name] = forms.EmailField(
                    widget=forms.EmailInput(
                        attrs={
                            'class':'form-control'
                        }
                    )
                )
                if response:
                    self.fields[field_name].initial = response
            elif field.type=='2':
                self.fields[field_name] = forms.DateField(
                    widget=forms.DateInput(
                        attrs={
                            'class':'form-control mydatepicker'
                        },
                        format='%d/%m/%Y'
                    ),
                    input_formats=('%d/%m/%Y', )
                )
                if response:
                    response = datetime.strptime(
                        response, '%Y-%m-%d'
                    )
                    response = datetime.strftime(
                        response, '%d/%m/%Y'
                    )
                    self.fields[field_name].initial = response
            else:
                self.fields[field_name] = forms.CharField(
                    widget=forms.TextInput(
                        attrs={
                            'class':'form-control'
                        }
                    )
                )
                if response:
                    self.fields[field_name].initial = response


    def save(self):
        super().save(commit=False)
        event=self.instance
        event.company = self.request.user.company
        if not event.token:
            event.token = token_urlsafe(32)
        event.save()

        extra_fields = _models.EventExtraField.objects.filter(
            company=self.request.user.company
        )

        for field in extra_fields:
            extra_field_response = models.EventExtraFieldResponse.objects.get_or_create(
                event=event,
                question=field
            )[0]

            extra_field_response.response=self.cleaned_data.get(field.name)
            extra_field_response.save()
        return event

    class Meta:
        model = models.Event
        fields = (
            'event_name',
            'venue',
            'start_date',
            'end_date',
        )

class EventHTMLForm(forms.ModelForm):
    x = forms.FloatField(
        required=False,
        widget=forms.HiddenInput()
    )
    y = forms.FloatField(
        required=False,
        widget=forms.HiddenInput()
    )
    width = forms.FloatField(
        required=False,
        widget=forms.HiddenInput()
    )
    height = forms.FloatField(
        required=False,
        widget=forms.HiddenInput()
    )

    x_poster = forms.FloatField(
        required=False,
        widget=forms.HiddenInput()
    )
    y_poster = forms.FloatField(
        required=False,
        widget=forms.HiddenInput()
    )
    width_poster = forms.FloatField(
        required=False,
        widget=forms.HiddenInput()
    )
    height_poster = forms.FloatField(
        required=False,
        widget=forms.HiddenInput()
    )

    x_spost = forms.FloatField(
        required=False,
        widget=forms.HiddenInput()
    )
    y_spost = forms.FloatField(
        required=False,
        widget=forms.HiddenInput()
    )
    width_spost = forms.FloatField(
        required=False,
        widget=forms.HiddenInput()
    )
    height_spost = forms.FloatField(
        required=False,
        widget=forms.HiddenInput()
    )

    x_logo_bg = forms.FloatField(
        required=False,
        widget=forms.HiddenInput()
    )
    y_logo_bg = forms.FloatField(
        required=False,
        widget=forms.HiddenInput()
    )
    width_logo_bg = forms.FloatField(
        required=False,
        widget=forms.HiddenInput()
    )
    height_logo_bg = forms.FloatField(
        required=False,
        widget=forms.HiddenInput()
    )

    x_footer_bg = forms.FloatField(
        required=False,
        widget=forms.HiddenInput()
    )
    y_footer_bg = forms.FloatField(
        required=False,
        widget=forms.HiddenInput()
    )
    width_footer_bg = forms.FloatField(
        required=False,
        widget=forms.HiddenInput()
    )
    height_footer_bg = forms.FloatField(
        required=False,
        widget=forms.HiddenInput()
    )

    def save(self):
        event = super().save()

        # Get image cropper data
        x = self.cleaned_data.get('x')
        y = self.cleaned_data.get('y')
        w = self.cleaned_data.get('width')
        h = self.cleaned_data.get('height')

        if x and y and w and h:
            logo = Image.open(event.event_logo)
            cropped_logo = logo.crop((x, y, w+x, h+y))
            cropped_logo.save(event.event_logo.path)

        x_poster = self.cleaned_data.get('x_poster')
        y_poster = self.cleaned_data.get('y_poster')
        width_poster = self.cleaned_data.get('width_poster')
        height_poster = self.cleaned_data.get('height_poster')

        if x_poster and y_poster and width_poster and height_poster:
            poster = Image.open(event.event_poster_image)
            cropped_poster = poster.crop(
                (x_poster, y_poster,
                x_poster+width_poster, y_poster+height_poster)
            )
            cropped_poster.save(event.event_poster_image.path)

        x_spost = self.cleaned_data.get('x_spost')
        y_spost = self.cleaned_data.get('y_spost')
        width_spost = self.cleaned_data.get('width_spost')
        height_spost = self.cleaned_data.get('height_spost')

        if x_spost and y_spost and width_spost and height_spost:
            sposter = Image.open(event.event_slogan_poster)
            cropped_sposter = sposter.crop(
                (x_spost, y_spost,
                width_spost+x_spost, height_spost+y_spost)
            )
            cropped_sposter.save(event.event_slogan_poster.path)

        x_logo_bg = self.cleaned_data.get('x_logo_bg')
        y_logo_bg = self.cleaned_data.get('y_logo_bg')
        width_logo_bg = self.cleaned_data.get('width_logo_bg')
        height_logo_bg = self.cleaned_data.get('height_logo_bg')

        if x_logo_bg and y_logo_bg and width_logo_bg and height_logo_bg:
            print('working')
            poster_bg = Image.open(event.poster_image_background)
            cropped_poster_bg = poster_bg.crop(
                (x_logo_bg, y_logo_bg,
                width_logo_bg+x_logo_bg, height_logo_bg+y_logo_bg)
            )
            cropped_poster_bg.save(event.poster_image_background.path)

        x_footer_bg = self.cleaned_data.get('x_footer_bg')
        y_footer_bg = self.cleaned_data.get('y_footer_bg')
        width_footer_bg = self.cleaned_data.get('width_footer_bg')
        height_footer_bg = self.cleaned_data.get('height_footer_bg')

        if (x_footer_bg and y_footer_bg and width_footer_bg and height_footer_bg):
            footer_bg = Image.open(event.footer_background)
            cropped_footer_bg = footer_bg.crop(
                (x_footer_bg, y_footer_bg,
                width_footer_bg+x_footer_bg, y_footer_bg+height_footer_bg)
            )
            cropped_footer_bg.save(event.footer_background.path)

        speakers = event.speakers.all()
        speakers = speakers.filter(is_active=True, in_tile=True)

        speakers_img_path_list = []
        if event.event_poster_image and event.event_logo:
            cropped_poster = Image.open(event.event_poster_image)
            cropped_logo = Image.open(event.event_logo)

            for speaker in speakers:
                poster_title = event.poster_title
                name = speaker.name
                image = Image.open(speaker.image)
                speakers_img_path_list.append(speaker.image.path)
                speaker.fb_image = utils.fb_image(image, cropped_poster, cropped_logo, name, poster_title)
                speaker.ig_image = utils.ig_image(image, cropped_poster, cropped_logo, name, poster_title)
                speaker.story_image = utils.story_image(image, cropped_poster, cropped_logo, name, poster_title)
                speaker.save()
        if event.speaker_tile:
            path = event.speaker_tile.path
        else:
            path = None
        event.speaker_tile = utils.photo_collage(
            speakers_img_path_list,
            event.images_per_row,
            event.event_name,
            path=path
        )
        event.save()

    class Meta:
        model = models.Event
        fields = (
            'main_title',
            'event_logo',
            'tickets_available',
            'promotion_period',
            'promotional_text',
            'email_copy',
            'event_slogan_poster',
            'event_poster_image',
            'host_title',
            'poster_title',
            'images_per_row',
            'poster_image_background',
            'footer_background',
        )
        widgets = {
            'main_title': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'tickets_available': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'promotion_period': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'promotional_text': forms.Textarea(
                attrs={
                    'class': 'form-control noresize',
                    'rows': '5'
                }
            ),
            'email_copy': forms.Textarea(
                attrs={
                    'class': 'form-control noresize',
                    'rows': '5'
                }
            ),
            'event_slogan_poster': forms.ClearableFileInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'event_logo': forms.ClearableFileInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'event_poster_image': forms.ClearableFileInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'host_title': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'poster_title': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'images_per_row': forms.NumberInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'poster_image_background': forms.ClearableFileInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'footer_background': forms.ClearableFileInput(
                attrs={
                    'class': 'form-control'
                }
            ),
        }

class EventPassForm(forms.ModelForm):

    class Meta:
        model = models.EventPass
        fields = (
            'name',
            'price'
        )
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'price': forms.NumberInput(
                attrs={
                    'class': 'form-control'
                }
            ),
        }

# formset of event passes
pass_formset = forms.inlineformset_factory(
    models.Event,
    models.EventPass,
    form=EventPassForm,
    extra=1
)

class EventSessionForm(forms.ModelForm):

    class Meta:
        model = models.EventSession
        fields = (
            'day_or_session',
            'title',
            'subtitle'
        )
        widgets = {
            'day_or_session': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'title': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'subtitle': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
        }

# formset of event sessions
session_formset = forms.inlineformset_factory(
    models.Event,
    models.EventSession,
    form=EventSessionForm,
    extra=1
)

class EventSpeakerForm(forms.ModelForm):
    x = forms.FloatField(widget=forms.HiddenInput())
    y = forms.FloatField(widget=forms.HiddenInput())
    width = forms.FloatField(widget=forms.HiddenInput())
    height = forms.FloatField(widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        self.event_obj = kwargs.pop('event_obj', None)
        speaker = kwargs.get('instance', None)
        super().__init__(*args, **kwargs)

        extra_fields = models.AdditionalSpeakerField.objects.filter(
            event=self.event_obj
        )

        for field in extra_fields:
            field_name = field.name

            # Fill with initial values if form is getting updated
            response = None
            if speaker:
                response = models.AdditionalSpeakerFieldResponse.objects.filter(
                    speaker=speaker,
                    field=field
                )
                if response:
                    response = response.first().response

            if field.type=='4':
                choice_raw_list = [str.strip() for str in field.dropdown_choices.split(',')]
                CHOICE_LIST = []
                for i, choice in enumerate(choice_raw_list):
                    CHOICE_LIST.append((choice, choice))

                self.fields[field_name] = forms.ChoiceField(
                    required=False,
                    choices=CHOICE_LIST,
                    widget=forms.Select(
                        attrs={
                            'class':'form-control custom-select'
                        }
                    )
                )
                if response:
                    self.fields[field_name].initial = response
            elif field.type=='3':
                self.fields[field_name] = forms.EmailField(
                    required=False,
                    widget=forms.EmailInput(
                        attrs={
                            'class':'form-control'
                        }
                    )
                )
                if response:
                    self.fields[field_name].initial = response
            elif field.type=='2':
                self.fields[field_name] = forms.DateField(
                    required=False,
                    widget=forms.DateInput(
                        attrs={
                            'class':'form-control mydatepicker'
                        },
                        format='%d/%m/%Y'
                    ),
                    input_formats=('%d/%m/%Y', )
                )
                if response:
                    response = datetime.strptime(
                        response, '%Y-%m-%d'
                    )
                    response = datetime.strftime(
                        response, '%d/%m/%Y'
                    )
                    self.fields[field_name].initial = response
            else:
                self.fields[field_name] = forms.CharField(
                    required=False,
                    widget=forms.TextInput(
                        attrs={
                            'class':'form-control'
                        }
                    )
                )
                if response:
                    self.fields[field_name].initial = response


    def save(self):
        super().save(commit=False)
        speaker=self.instance
        speaker.event = self.event_obj

        # Save speaker with cropped image
        speaker.save()

        # Get image cropper data
        x = self.cleaned_data.get('x')
        y = self.cleaned_data.get('y')
        w = self.cleaned_data.get('width')
        h = self.cleaned_data.get('height')

        image = Image.open(speaker.image)
        cropped_image = image.crop((x, y, w+x, h+y))
        cropped_image.save(speaker.image.path)
        logo = Image.open(self.event_obj.event_logo)
        poster = Image.open(self.event_obj.event_poster_image)
        poster_title = self.event_obj.poster_title
        name = speaker.name
        speaker.fb_image = utils.fb_image(cropped_image, poster, logo, name, poster_title)
        speaker.ig_image = utils.ig_image(cropped_image, poster, logo, name, poster_title)
        speaker.story_image = utils.story_image(cropped_image, poster, logo, name, poster_title)
        speaker.save()

        extra_fields = models.AdditionalSpeakerField.objects.filter(
            event=self.event_obj
        )

        for field in extra_fields:
            extra_field_response = models.AdditionalSpeakerFieldResponse.objects.get_or_create(
                speaker=speaker,
                field=field
            )[0]

            extra_field_response.response=self.cleaned_data.get(field.name)
            extra_field_response.save()

        speakers = models.EventSpeaker.objects.filter(
            event=self.event_obj,
            is_active=True
        )
        print(speakers)

        speakers_img_path_list = [
            speaker.image.path for speaker in speakers
        ]

        if self.event_obj.speaker_tile:
            path = self.event_obj.speaker_tile.path
            print(path)
        else:
            path = None
        self.event_obj.speaker_tile = utils.photo_collage(
            speakers_img_path_list,
            self.event_obj.images_per_row,
            self.event_obj.event_name,
            path=path
        )
        self.event_obj.save()

        # Return model object
        return speaker

    class Meta:
        model = models.EventSpeaker
        fields = (
            'name',
            'email',
            'contact',
            'profession',
            'organisation',
            'image'
        )
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'email': forms.EmailInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'contact': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'profession': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'organisation': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'image': forms.FileInput(
                attrs={
                    'class': 'form-control'
                }
            )
        }

class EventSpeakerUpdateForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.event_obj = kwargs.pop('event_obj', None)
        speaker = kwargs.get('instance', None)
        super().__init__(*args, **kwargs)

        self.fields['session'] = forms.ModelChoiceField(
            queryset=models.EventSession.objects.filter(
                event=self.event_obj
            ),
            widget=forms.Select(
                attrs={
                    'class': 'form-control'
                }
            ),
            empty_label=None
        )

        extra_fields = models.AdditionalSpeakerField.objects.filter(
            event=self.event_obj
        )

        for field in extra_fields:
            field_name = field.name

            # Fill with initial values if form is getting updated
            response = None
            if speaker:
                response = models.AdditionalSpeakerFieldResponse.objects.filter(
                    speaker=speaker,
                    field=field
                )
                if response:
                    response = response.first().response

            if field.type=='4':
                choice_raw_list = [str.strip() for str in field.dropdown_choices.split(',')]
                CHOICE_LIST = []
                for i, choice in enumerate(choice_raw_list):
                    CHOICE_LIST.append((choice, choice))

                self.fields[field_name] = forms.ChoiceField(
                    required=False,
                    choices=CHOICE_LIST,
                    widget=forms.Select(
                        attrs={
                            'class':'form-control custom-select'
                        }
                    )
                )
                if response:
                    self.fields[field_name].initial = response
            elif field.type=='3':
                self.fields[field_name] = forms.EmailField(
                    required=False,
                    widget=forms.EmailInput(
                        attrs={
                            'class':'form-control'
                        }
                    )
                )
                if response:
                    self.fields[field_name].initial = response
            elif field.type=='2':
                self.fields[field_name] = forms.DateField(
                    required=False,
                    widget=forms.DateInput(
                        attrs={
                            'class':'form-control mydatepicker'
                        },
                        format='%d/%m/%Y'
                    ),
                    input_formats=('%d/%m/%Y', )
                )
                if response:
                    response = datetime.strptime(
                        response, '%Y-%m-%d'
                    )
                    response = datetime.strftime(
                        response, '%d/%m/%Y'
                    )
                    self.fields[field_name].initial = response
            else:
                self.fields[field_name] = forms.CharField(
                    required=False,
                    widget=forms.TextInput(
                        attrs={
                            'class':'form-control'
                        }
                    )
                )
                if response:
                    self.fields[field_name].initial = response


    def save(self):
        super().save()
        speaker=self.instance

        extra_fields = models.AdditionalSpeakerField.objects.filter(
            event=self.event_obj
        )

        for field in extra_fields:
            extra_field_response = models.AdditionalSpeakerFieldResponse.objects.get_or_create(
                speaker=speaker,
                field=field
            )[0]

            extra_field_response.response=self.cleaned_data.get(field.name)
            extra_field_response.save()

        # Return model object
        return speaker

    class Meta:
        model = models.EventSpeaker
        fields = (
            'name',
            'email',
            'contact',
            'profession',
            'organisation',
            'session',
            'chat_host',
            'in_tile',
            'is_active'
        )
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'email': forms.EmailInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'contact': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'profession': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'organisation': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'chat_host': forms.CheckboxInput(
                attrs={
                    'data-size': 'normal',
                    'data-on-color': 'primary',
                    'data-off-color': 'default',
                    'data-on-text': 'Yes',
                    'data-off-text': 'No',
                }
            ),
            'in_tile': forms.CheckboxInput(
                attrs={
                    'data-size': 'normal',
                    'data-on-color': 'primary',
                    'data-off-color': 'default',
                    'data-on-text': 'Yes',
                    'data-off-text': 'No',
                }
            ),
            'is_active': forms.CheckboxInput(
                attrs={
                    'data-size': 'normal',
                    'data-on-color': 'primary',
                    'data-off-color': 'default',
                    'data-on-text': 'Active',
                    'data-off-text': 'Inactive',
                }
            )
        }

class AdditionalSpeakerFieldForm(forms.ModelForm):

    class Meta:
        model = models.AdditionalSpeakerField
        fields = (
            'name',
            'type',
            'dropdown_choices'
        )
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'type':forms.Select(
                attrs={
                    'class': 'form-control custom-select'
                }
            ),
            'dropdown_choices': forms.Textarea(
                attrs={
                    'class': 'form-control noresize',
                    'readonly': True,
                    'rows': 2
                }
            )
        }

ExtraSpeakerFieldFormset = forms.inlineformset_factory(
    models.Event,
    models.AdditionalSpeakerField,
    form=AdditionalSpeakerFieldForm,
    extra=1
)
