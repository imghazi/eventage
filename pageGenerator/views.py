from django.shortcuts import render, reverse, redirect
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView, UpdateView, FormView
from django.contrib.sites.shortcuts import get_current_site
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse_lazy

from urllib.parse import quote

from . import models, forms
from accounts.mixins import SameCompanyOnlyMixin


# Create your views here.
class EventCreateView(LoginRequiredMixin, CreateView):
    model = models.Event
    form_class = forms.EventForm
    template_name = 'pageGenerator/event_form.html'

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        kwargs['request'] = self.request
        return kwargs

class EventUpdateView(LoginRequiredMixin, UpdateView):
    model = models.Event
    form_class = forms.EventForm
    template_name = 'pageGenerator/event_update_form.html'
    context_object_name = 'event'

class EventListView(LoginRequiredMixin, ListView):
    model = models.Event
    template_name = 'pageGenerator/event_list.html'
    context_object_name = 'events'

    def get_queryset(self):
        return models.Event.objects.filter(
            company=self.request.user.company,
            is_active=True
        )

class EventDetailView(LoginRequiredMixin,
    SameCompanyOnlyMixin, DetailView):
    model = models.Event
    template_name = 'pageGenerator/event_detail.html'
    context_object_name = 'event'

    def post(self, request, *args, **kwargs):

        event = self.get_object()
        if 'update_event' in request.POST:
            event_form = forms.EventForm(
                request.POST,
                instance=event,
                request=self.request
            )
            event_html_form = forms.EventHTMLForm(
                request.POST,
                request.FILES,
                instance=event
            )
            passes = models.EventPass.objects.filter(
                event=event,
                is_active=True
            )
            pass_formset = forms.pass_formset(
                request.POST,
                instance=event,
                queryset=passes,
                prefix='pass'
            )
            sessions = models.EventSession.objects.filter(
                event=event,
                is_active=True
            )
            session_formset = forms.session_formset(
                request.POST,
                instance=event,
                queryset=sessions,
                prefix='session'
            )
            # print(event_html_form)
            if all({
                event_form.is_valid(),
                event_html_form.is_valid(),
                pass_formset.is_valid(),
                session_formset.is_valid()
            }):
                event_form.save()
                event_html_form.save()
                pass_formset.save()
                session_formset.save()

                request.session['active_tab'] = 1
                request.session['event_form_active'] = 0
            else:
                request.session['active_tab'] = 1
                request.session['event_form_active'] = 1
                print('forms not valid')
        elif 'extra_speaker_fields' in request.POST:
            formset = forms.ExtraSpeakerFieldFormset(
                request.POST,
                instance=event,
                queryset=models.AdditionalSpeakerField.objects.filter(
                    event=event
                ),
                prefix='extra_speaker_field'
            )
            if formset.is_valid():
                formset.save()
                request.session['active_tab'] = 2

        return redirect(event)

    def get_context_data(self, *args, **kwargs):
        data = super().get_context_data(*args, **kwargs)

        # set the active tab in session
        if 'active_tab' not in self.request.session:
            self.request.session['active_tab'] = 1

        if 'event_form_active' not in self.request.session:
            self.request.session['event_form_active'] = 0

        event = self.get_object()
        data['event_form'] = forms.EventForm(
            instance=event,
            request=self.request
        )
        data['event_html_form'] = forms.EventHTMLForm(
            instance=event
        )
        data['passes'] = models.EventPass.objects.filter(
            event=event,
            is_active=True
        )
        data['pass_formset'] = forms.pass_formset(
            instance=event,
            queryset=data['passes'],
            prefix='pass'
        )
        data['sessions'] = models.EventSession.objects.filter(
            event=event,
            is_active=True
        )
        data['session_formset'] = forms.session_formset(
            instance=event,
            queryset=data['sessions'],
            prefix='session'
        )
        data['extra_fields'] = models.EventExtraFieldResponse.objects.filter(
            event=event
        )
        data['speaker_form'] = forms.EventSpeakerForm(
            event_obj=event,
        )
        data['extra_speaker_field_formset'] = forms.ExtraSpeakerFieldFormset(
            instance=event,
            queryset=models.AdditionalSpeakerField.objects.filter(
                event=event
            ),
            prefix='extra_speaker_field'
        )
        data['e_speakers'] = models.EventSpeaker.objects.filter(
            event=event,
            is_active=True
        )
        # print(data['e_speakers'])
        return data

class SpeakerCreateView(FormView):
    template_name = 'pageGenerator/speaker-form.html'
    form_class = forms.EventSpeakerForm
    success_url = reverse_lazy('thankyou')

    def form_valid(self, form):
        form.save()

        return super().form_valid(form)

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        kwargs['event_obj'] = models.Event.objects.get(
            token=self.kwargs['token']
        )
        return kwargs

    def get_context_data(self, *args, **kwargs):
        data = super().get_context_data(*args, **kwargs)
        data['event'] = models.Event.objects.get(
            token=self.kwargs['token']
        )
        return data

class SpeakerUpdateView(LoginRequiredMixin, UpdateView):
    form_class = forms.EventSpeakerUpdateForm
    model = models.EventSpeaker
    template_name = 'pageGenerator/speaker_update_form.html'
    context_object_name = 'speaker'

    def form_valid(self, form):
        form.save()
        self.request.session['active_tab'] = 3
        return super().form_valid(form)

    def get_success_url(self):
        return reverse(
            'pageGenerator:event-detail',
            kwargs={'pk':self.get_object().event.pk}
        )

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        speaker = self.get_object()
        kwargs['event_obj'] = speaker.event
        return kwargs

    def get_context_data(self, *args, **kwargs):
        data = super().get_context_data(*args, **kwargs)
        self.request.session['active_tab'] = 3
        return data

class HTMLPageView(TemplateView):
    template_name = 'pageGenerator/htmlpage.html'

    def get_context_data(self, *args, **kwargs):
        data = super().get_context_data(*args, **kwargs)
        data['event'] = models.Event.objects.get(
            pk=self.kwargs['pk']
        )
        data['domain'] = get_current_site(self.request).domain
        return data

def fetch_image(request, pk, img):
    try:
        speaker = models.EventSpeaker.objects.get(pk=pk)
        data = {
            'success': True,
            'pk': pk
        }
        if img==1:
            data['image'] = quote(
                '/media/'+str(speaker.fb_image)
            )
        elif img==2:
            data['image'] = quote(
                '/media/'+str(speaker.ig_image)
            )
        elif img==3:
            data['image'] = quote(
                '/media/'+str(speaker.story_image)
            )
        print(data['image'])
    except models.EventSpeaker.DoesNotExist:
        print('Speaker does not exist!')
        data = {
            'success': False
        }
    return JsonResponse(data)
