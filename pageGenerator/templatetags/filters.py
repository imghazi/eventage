from django import template

register = template.Library()

@register.filter('input_type')
def input_type(input):
    return input.field.widget.__class__.__name__

@register.filter('get_fields')
def get_fields(instance):
    return instance._meta.get_fields()

@register.filter('contains')
def contains(parent_string, sub_string):
    return sub_string in parent_string

@register.filter('chat_hosts')
def chat_hosts(queryset):
    return queryset.filter(chat_host=True)

@register.filter('not_chat_host')
def not_chat_host(obj):
    return obj.chat_host

@register.filter('is_active')
def is_active(queryset):
    return queryset.filter(is_active=True)
