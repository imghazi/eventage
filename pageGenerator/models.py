from django.db import models
from django.shortcuts import reverse
from django.db.models.signals import post_save
from django.dispatch import receiver

from datetime import datetime

from accounts import models as _models
# Create your models here.

class Event(models.Model):
    event_name = models.CharField(max_length=100, blank=False)
    venue = models.TextField(blank=True)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    company = models.ForeignKey(
        _models.Company,
        related_name='events',
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )
    is_active = models.BooleanField(default=True)
    token = models.CharField(
        max_length=100,
        unique=True,
        null=True
    )
    images_per_row = models.PositiveIntegerField(
        default=5,
        blank=False,
        verbose_name='Images Per Row In Tile'
    )
    # html specific fields
    main_title = models.CharField(
        max_length=100,
        default='Thank You for Sharing Your Experience'
    )
    tickets_available = models.CharField(
        max_length=100,
        default='January 1'
    )
    promotion_period = models.CharField(
        max_length=200,
        default='January 1 - January 10'
    )
    promotional_text = models.TextField(
        default='<p>That means when your community members purchase the upgrade package at <strong>$XXX</strong> during the Default Pass purchasing period, you will receive $XXX for each course sold. </p> When members of your community purchases the upgrade package priced at $XXX during the Last Chance purchasing period, you will receive $XXX for each course sold. You can use the copy, images and Social Media posts below without using an Affiliate link of any kind.'
    )
    email_copy = models.TextField(
        default='If you are an Affiliate, you must insert your unique tracking link into the text where it says [ INSERT AFFILIATE LINK...]. If you don’t include your link, we will not be able to track clicks or pay commissions. If you are not an Affiliate, please feel free to delete all of the references to the links.'
    )
    event_slogan_poster = models.ImageField(
        upload_to='pageGenerator/event_slogan_poster/',
        default='pageGenerator/event_slogan_poster/default.jpg'
    )
    event_logo = models.ImageField(
        upload_to='pageGenerator/event_logo/',
        default='pageGenerator/event_logo/event.png',
    )
    event_poster_image = models.ImageField(
        upload_to='pageGenerator/event_poster_image/',
        default='pageGenerator/event_poster_image/default.jpeg'
    )
    host_title = models.CharField(
        max_length=100,
        default='Event Host'
    )
    poster_title = models.CharField(
        max_length=100,
        default='Jan 12-14: Wisdom in times such as these'
    )
    speaker_tile = models.ImageField(
        upload_to='pageGenerator/tile/',
        default='pageGenerator/tile/default.jpeg'
    )
    poster_image_background = models.ImageField(
        upload_to='pageGenerator/logo_background/',
        default='pageGenerator/logo_background/default.jpg'
    )
    footer_background = models.ImageField(
        upload_to='pageGenerator/footer_image/',
        default='pageGenerator/footer_image/default.jpeg'
    )

    def __str__(self):
        return self.event_name

    def get_absolute_url(self):
        return reverse(
            'pageGenerator:event-detail',
            kwargs={'pk': self.pk}
        )

class EventPass(models.Model):
    event = models.ForeignKey(
        Event,
        related_name='passes',
        on_delete=models.CASCADE
    )
    name = models.CharField(
        max_length=100,
        blank=False,
        verbose_name='Pass Name'
    )
    price = models.PositiveIntegerField(
        blank=False
    )
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

class EventSession(models.Model):
    event = models.ForeignKey(
        Event,
        related_name='sessions',
        on_delete=models.CASCADE
    )
    day_or_session = models.CharField(
        max_length=100,
        blank=False
    )
    title = models.CharField(
        max_length=100,
        blank=False
    )
    subtitle = models.CharField(
        max_length=100,
        blank=False
    )
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.day_or_session

class EventExtraFieldResponse(models.Model):
    event = models.ForeignKey(
        Event,
        related_name='extra_fields',
        on_delete=models.CASCADE
    )
    question = models.ForeignKey(
        _models.EventExtraField,
        related_name='responses',
        on_delete=models.CASCADE
    )
    response = models.CharField(
        max_length=200,
        blank=True
    )

    def __str__(self):
        if self.question.type == '2':
            return datetime.strftime(datetime.strptime(
                self.response, '%Y-%m-%d'
            ), '%b %d, %Y')
        return self.response

class Speaker(models.Model):
    name = models.CharField(
        max_length=100,
        blank=False
    )
    email = models.CharField(
        max_length=100,
        blank=True
    )
    contact = models.CharField(
        max_length=13,
        blank=True
    )
    profession = models.CharField(
        max_length=100,
        blank=True
    )
    organisation = models.CharField(
        max_length=100,
        blank=True
    )
    image = models.ImageField(
        verbose_name='Image for posters',
        upload_to='pageGenerator/speaker_image/',
        blank=False
    )

    def __str__(self):
        return self.name

class EventSpeaker(models.Model):
    event = models.ForeignKey(
        Event,
        related_name='speakers',
        on_delete=models.CASCADE,
    )
    session = models.ForeignKey(
        EventSession,
        related_name='speakers',
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )
    name = models.CharField(
        max_length=100,
        blank=False
    )
    email = models.EmailField(
        max_length=100,
        blank=True
    )
    contact = models.CharField(
        max_length=13,
        blank=True
    )
    profession = models.CharField(
        max_length=100,
        blank=True
    )
    organisation = models.CharField(
        max_length=100,
        blank=True
    )
    image = models.ImageField(
        verbose_name='Image for posters',
        upload_to='pageGenerator/speaker_image/',
        default='pageGenerator/speaker_image/images.jpeg'
    )
    fb_image = models.ImageField(
        upload_to='pageGenerator/fb/',
        default='pageGenerator/fb/default.jpeg'
    )
    ig_image = models.ImageField(
        upload_to='pageGenerator/ig/',
        default='pageGenerator/ig/default.jpeg'
    )
    story_image = models.ImageField(
        upload_to='pageGenerator/story/',
        default='pageGenerator/story/default.jpeg'
    )
    chat_host = models.BooleanField(default=False)
    is_active = models.BooleanField(
        default=True,
        verbose_name='Status'
    )
    in_tile = models.BooleanField(
        default=True,
        verbose_name='In Speakers Tile?'
    )

    def __str__(self):
        return self.name

@receiver(post_save, sender=Event)
def create_defaults(sender, instance, created, **kwargs):
    if created:
        default_pass = EventPass.objects.create(
            event=instance,
            name='Default Pass',
            price=250
        )
        default_session = EventSession.objects.create(
            event=instance,
            day_or_session='Day 1',
            title='Introduction to Event: First Session',
            subtitle='First Session: January 12 starts at 12:00 EST'
        )
        default_speaker = EventSpeaker.objects.create(
            event=instance,
            session=default_session,
            name='Default User',
        )
        chat_host = EventSpeaker.objects.create(
            event=instance,
            session=default_session,
            name='Default User',
            chat_host=True
        )

class AdditionalSpeakerField(models.Model):
    event = models.ForeignKey(
        Event,
        related_name='additional_speaker_fields',
        on_delete=models.CASCADE
    )
    name = models.CharField(
        max_length=100,
        blank=False
    )
    type = models.CharField(
        max_length=100,
        choices=_models.FIELD_TYPE_CHOICES,
        default='1'
    )
    dropdown_choices = models.TextField(blank=True)

    def __Str__(self):
        return self.name

class AdditionalSpeakerFieldResponse(models.Model):
    field = models.ForeignKey(
        AdditionalSpeakerField,
        related_name='responses',
        on_delete=models.CASCADE
    )
    speaker = models.ForeignKey(
        EventSpeaker,
        related_name='additional_field_response',
        on_delete=models.CASCADE
    )
    response = models.CharField(
        max_length=100,
        blank=False
    )
