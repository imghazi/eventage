from django.contrib import admin

from . import models

# Register your models here.
admin.site.register(models.Event)
admin.site.register(models.EventPass)
admin.site.register(models.EventSession)
admin.site.register(models.EventExtraFieldResponse)
admin.site.register(models.AdditionalSpeakerField)
admin.site.register(models.Speaker)
admin.site.register(models.EventSpeaker)
admin.site.register(models.AdditionalSpeakerFieldResponse)
