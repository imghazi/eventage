from django.conf import settings

# import cv2
from PIL import Image, ImageFilter, ImageDraw, ImageFont
import numpy as np

from datetime import datetime
from math import ceil

NAME_FONT = 'DejaVuSans-Bold.ttf'

def text_wrap(text, font, max_width):
    lines = []
    # If the width of the text is smaller than image width
    # we don't need to split it, just add it to the lines array
    # and return
    if font.getsize(text)[0] <= max_width:
        lines.append(text)
    else:
        # split the line by spaces to get words
        words = text.split(' ')
        i = 0
        # append every word to a line while its width is shorter than image width
        while i < len(words):
            line = ''
            while i < len(words) and font.getsize(line + words[i])[0] <= max_width:
                line = line + words[i] + " "
                i += 1
            if not line:
                line = words[i]
                i += 1
            # when the line gets longer than the max width do not append the word,
            # add the line to the lines array
            lines.append(line)
    return lines

def draw_watermark(img, opacity):

    watermark = Image.new("RGBA", img.size)

    waterdraw = ImageDraw.ImageDraw(watermark, "RGBA")

    w = img.size[0]
    h = img.size[1]

    waterdraw.polygon((0, 0, w, 0, w, h, 0, h), fill='whitesmoke')

    watermark.putalpha(opacity)

    img.paste(watermark, None, watermark)

def crop_from_middle(img, w):
    wi = img.size[0]
    hi = img.size[1]

    x1 = abs(w - wi)/2
    y1 = 0
    x2 = wi - abs(w-wi)/2
    y2 = hi

    return img.crop((x1, y1, x2, y2))

def write_text(img, lines, pos, bandwidth, x_off, color, font):

    draw = ImageDraw.Draw(img)

    for i, line in enumerate(lines):
        draw.text((x_off, pos+i*bandwidth), line, fill=color, font=font)

    return img

def write_text_w_bg(img, pos, bandwidth, x_off, bg_color, text, color, font):

    lines = text_wrap(text, font, img.size[0]-2*x_off)

    bg = Image.new('RGBA', (img.size[0], bandwidth*len(lines)+20), bg_color)

    bg = write_text(bg, lines, 10, bandwidth, x_off, color, font)

    img.paste(bg, (0, pos - bandwidth*(len(lines) - 1)))

def fb_image(img1, img2, logo, name, poster_title):
    size1 = img1.size
    size2 = img2.size

    img1 = img1.resize(
        (419, 419),
        Image.ANTIALIAS
    )
    img2 = img2.resize(
        (419, 419),
        Image.ANTIALIAS
    )
    # img1 = img1.resize((800, 419), Image.ANTIALIAS)
    # img2 = img2.resize((800, 419), Image.ANTIALIAS)

    img1 = crop_from_middle(img1, 375)
    img2 = crop_from_middle(img2, 425)

    draw_watermark(img2, 85)

    logo = logo.resize((200, 75), Image.ANTIALIAS)

    img2.paste(logo, (20, 340))

    names = text_wrap(name, ImageFont.truetype(NAME_FONT, 60), img2.size[0]-2*20)

    write_text(img2, names, 10, 70, 20, 'navy', ImageFont.truetype('Vera.ttf', 70))

    # convert pil to cv2
    img1 = np.array(img1)
    img2 = np.array(img2)

    # Add side by side
    vis = np.concatenate((img2, img1), axis=1)

    # convert cv2 to pil
    f_img = Image.fromarray(vis)

    write_text_w_bg(f_img, 275, 30, 20, 'indigo', poster_title, 'white', ImageFont.truetype('Vera.ttf', 30))

    file_name = settings.MEDIA_ROOT+'/pageGenerator/fb/'+name+datetime.now().strftime('%Y_%m_%d__%H_%M_%S')+'.jpeg'

    f_img.save(file_name, 'JPEG')
    return 'pageGenerator/fb/'+name+datetime.now().strftime('%Y_%m_%d__%H_%M_%S')+'.jpeg'

def ig_image(img1, img2, logo, name, poster_title):
    img1 = img1.resize((669, 669), Image.ANTIALIAS)
    img2 = img2.resize((669, 669), Image.ANTIALIAS)

    img1 = crop_from_middle(img1, 334)
    img2 = crop_from_middle(img2, 335)

    draw_watermark(img2, 85)

    logo = logo.resize((200, 75), Image.ANTIALIAS)

    img2.paste(logo, (20, 20))

    names = text_wrap(name, ImageFont.truetype(NAME_FONT, 50), img2.size[0]-2*20)

    write_text(img2, names, 120, 65, 20, 'navy', ImageFont.truetype('Vera.ttf', 65))

    # convert pil to cv2
    img1 = np.array(img1)
    img2 = np.array(img2)

    # Add side by side
    vis = np.concatenate((img2, img1), axis=1)

    # convert cv2 to pil
    f_img = Image.fromarray(vis)

    write_text_w_bg(f_img, 575, 28, 20, 'indigo', poster_title, 'white', ImageFont.truetype('Vera.ttf', 28))

    file_name = settings.MEDIA_ROOT+'/pageGenerator/ig/'+name+datetime.now().strftime('%Y_%m_%d__%H_%M_%S')+'.jpeg'

    f_img.save(file_name, 'JPEG')
    return 'pageGenerator/ig/'+name+datetime.now().strftime('%Y_%m_%d__%H_%M_%S')+'.jpeg'

def story_image(img1, img2, logo, name, poster_title):
    size1 = img1.size
    size2 = img2.size

    img1 = img1.resize(
        (669, 669),
        Image.ANTIALIAS
    )
    img2 = img2.resize(
        (669, 669),
        Image.ANTIALIAS
    )

    img1 = crop_from_middle(img1, 212)
    img2 = crop_from_middle(img2, 164)

    draw_watermark(img2, 85)

    logo = logo.resize((150, 50), Image.ANTIALIAS)

    img2.paste(logo, (5, 600))

    names = text_wrap(name, ImageFont.truetype(NAME_FONT, 35), img2.size[0]-2*20)

    write_text(img2, names, 120, 40, 5, 'navy', ImageFont.truetype('Vera.ttf', 40))

    # convert pil to cv2
    img1 = np.array(img1)
    img2 = np.array(img2)

    # Add side by side
    vis = np.concatenate((img2, img1), axis=1)

    # convert cv2 to pil
    f_img = Image.fromarray(vis)

    write_text_w_bg(f_img, 540, 20, 5, 'indigo', poster_title, 'white', ImageFont.truetype('Vera.ttf', 20))

    file_name = settings.MEDIA_ROOT+'/pageGenerator/story/'+name+datetime.now().strftime('%Y_%m_%d__%H_%M_%S')+'.jpeg'

    f_img.save(file_name, 'JPEG')
    return 'pageGenerator/story/'+name+datetime.now().strftime('%Y_%m_%d__%H_%M_%S')+'.jpeg'

# def concat_tile(im_list_2d):
#     return cv2.vconcat(
#         [cv2.hconcat(im_list_h) for im_list_h in im_list_2d]
#     )

def photo_collage(img_path_list, images_per_row, ev_name, path=None):
    # print(path)
    img_list = []
    width = 200*images_per_row
    height = 200*ceil(len(img_path_list)/images_per_row)
    img_tile = Image.new('RGB', (width, height), 'white')
    print(len(img_path_list))
    x = 0
    y = 0
    for i, img_path in enumerate(img_path_list):
        img = Image.open(img_path)
        img = img.resize((200, 200), Image.ANTIALIAS)
        # print(i)
        img_tile.paste(img, (x, y))
        x = x+200
        if x==width:
            x = 0
            y += 200
    #     temp.append(np.array(img))
    #     if i==0:
    #         # print(np.array(img))
    #         width = temp[0].shape[0]
    #         height = temp[0].shape[1]
    #     if len(temp)==images_per_row:
    #         img_list.append(temp)
    #         temp = []
    # if len(temp)!=0:
    #     num_white_images = images_per_row-len(temp)
    #
    #     while num_white_images!=0:
    #         blank_img = np.zeros((height,width,3), dtype=np.uint8)
    #         blank_img.fill(255)
    #         temp.append(blank_img)
    #         num_white_images -= 1
    # if len(temp) == images_per_row:
    #     img_list.append(temp)
    # im_tile = concat_tile(img_list)
    # img_tile = Image.fromarray(im_tile)
    if path==None:
        path = settings.MEDIA_ROOT+'/pageGenerator/tile/'+ev_name+'_tile.jpeg'
    img_tile.save(path, 'JPEG')
    # cv2.imwrite(settings.MEDIA_ROOT+'/'+path, im_tile)
    return 'pageGenerator/tile/'+ev_name+'_tile.jpeg'
