from django.apps import AppConfig


class PagegeneratorConfig(AppConfig):
    name = 'pageGenerator'
