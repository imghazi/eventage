# Generated by Django 2.2.7 on 2020-06-03 11:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdditionalSpeakerField',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('type', models.CharField(choices=[('1', 'String'), ('2', 'Date'), ('3', 'Email'), ('4', 'Dropdown')], default='1', max_length=100)),
                ('dropdown_choices', models.TextField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('event_name', models.CharField(max_length=100)),
                ('venue', models.TextField(blank=True)),
                ('start_date', models.DateField(blank=True, null=True)),
                ('end_date', models.DateField(blank=True, null=True)),
                ('is_active', models.BooleanField(default=True)),
                ('token', models.CharField(max_length=100, null=True, unique=True)),
                ('main_title', models.CharField(blank=True, max_length=100)),
                ('tickets_available', models.CharField(blank=True, max_length=100)),
                ('promotion_period', models.CharField(blank=True, max_length=200)),
                ('promotional_text', models.TextField(blank=True)),
                ('email_copy', models.TextField(blank=True)),
                ('event_slogan_poster', models.ImageField(blank=True, null=True, upload_to='pageGenerator/event_slogan_poster/')),
                ('event_logo', models.ImageField(blank=True, default='pageGenerator/event_logo/event.png', null=True, upload_to='pageGenerator/event_logo/')),
                ('event_poster_image', models.ImageField(blank=True, null=True, upload_to='pageGenerator/event_poster_image/')),
                ('host_title', models.CharField(blank=True, max_length=100)),
                ('company', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='events', to='accounts.Company')),
            ],
        ),
        migrations.CreateModel(
            name='EventSession',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('day_or_session', models.CharField(max_length=100)),
                ('title', models.CharField(max_length=100)),
                ('subtitle', models.CharField(max_length=100)),
                ('is_active', models.BooleanField(default=True)),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sessions', to='pageGenerator.Event')),
            ],
        ),
        migrations.CreateModel(
            name='Speaker',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('email', models.CharField(blank=True, max_length=100)),
                ('contact', models.CharField(blank=True, max_length=13)),
                ('profession', models.CharField(blank=True, max_length=100)),
                ('organisation', models.CharField(blank=True, max_length=100)),
                ('image', models.ImageField(upload_to='pageGenerator/speaker_image/', verbose_name='Image for posters')),
            ],
        ),
        migrations.CreateModel(
            name='EventSpeaker',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('email', models.EmailField(blank=True, max_length=100)),
                ('contact', models.CharField(blank=True, max_length=13)),
                ('profession', models.CharField(blank=True, max_length=100)),
                ('organisation', models.CharField(blank=True, max_length=100)),
                ('image', models.ImageField(upload_to='pageGenerator/speaker_image/', verbose_name='Image for posters')),
                ('fb_image', models.ImageField(blank=True, default='pageGenerator/event_logo/event.png', null=True, upload_to='pageGenerator/fb_image/')),
                ('ig_image', models.ImageField(blank=True, default='pageGenerator/event_logo/event.png', null=True, upload_to='pageGenerator/ig_image/')),
                ('story_image', models.ImageField(blank=True, default='pageGenerator/event_logo/event.png', null=True, upload_to='pageGenerator/story_image/')),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='speakers', to='pageGenerator.Event')),
                ('session', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='speakers', to='pageGenerator.EventSession')),
            ],
        ),
        migrations.CreateModel(
            name='EventPass',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Pass Name')),
                ('price', models.PositiveIntegerField()),
                ('is_active', models.BooleanField(default=True)),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='passes', to='pageGenerator.Event')),
            ],
        ),
        migrations.CreateModel(
            name='EventExtraFieldResponse',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('response', models.CharField(blank=True, max_length=200)),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='extra_fields', to='pageGenerator.Event')),
                ('question', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='responses', to='accounts.EventExtraField')),
            ],
        ),
        migrations.CreateModel(
            name='AdditionalSpeakerFieldResponse',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('response', models.CharField(max_length=100)),
                ('field', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='responses', to='pageGenerator.AdditionalSpeakerField')),
                ('speaker', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='additional_field_response', to='pageGenerator.EventSpeaker')),
            ],
        ),
        migrations.AddField(
            model_name='additionalspeakerfield',
            name='event',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='additional_speaker_fields', to='pageGenerator.Event'),
        ),
    ]
