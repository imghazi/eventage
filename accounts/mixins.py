from django.shortcuts import redirect

from . import models

class ManagerRequiredMixin:

    def has_permissions(self):
        # checks if user type is supervisor or manager
        return self.request.user.type == 's' or self.request.user.type == 'm'

    def dispatch(self, request, *args, **kwargs):
        if not self.has_permissions():
            return redirect('error_403')
        return super().dispatch(
            request, *args, **kwargs)

class SameCompanyOnlyMixin:

    def is_same_company(self):
        #checks if user, updating details, and the user, whose details are being updated, belong to same company
        return self.get_object().company == self.request.user.company

    def dispatch(self, request, *args, **kwargs):
        if not self.is_same_company():
            return redirect('error_403')
        return super().dispatch(request, *args, **kwargs)
