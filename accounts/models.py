from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.utils.translation import ugettext_lazy as _

from decimal import Decimal
from datetime import date, timedelta

# Create your models here.

FIELD_TYPE_CHOICES = [
    ('1', 'String'),
    ('2', 'Date'),
    ('3', 'Email'),
    ('4', 'Dropdown')
]

class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password"""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password"""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a Superuser with the given email and password"""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True')
        return self._create_user(email, password, **extra_fields)

class Company(models.Model):
    """Creates a Company with necessary details"""
    name = models.CharField(max_length=50, blank=False)
    logo = models.ImageField(
        upload_to='accounts/company_logo/',
        default='accounts/company_logo/logo.png'
    )
    address_line_1 = models.CharField(max_length=100, blank=True)
    address_line_2 = models.CharField(max_length=100, blank=True)
    city = models.CharField(max_length=50, blank=True)
    state = models.CharField(max_length=50, blank=True)
    country = models.CharField(max_length=50, blank=True)
    pincode = models.CharField(max_length=50, blank=True)

    # company activation/deactivation
    is_active = models.BooleanField(default=False)

    #company subscription date
    subscription_date = models.DateField(default=date.today() + timedelta(days=30))

    #currrency field
    currency = models.CharField(max_length=4, default='£')

    def __str__(self):
        return self.name


class User(AbstractUser):
    USER_TYPE_CHOICES = [
        ('0', 'Normal'),
        ('1', 'Manager'),
        ('s', 'Supervisor')
    ]

    username=None
    email = models.EmailField(unique=True, blank=False)
    first_name = models.CharField(max_length=50, blank=False)
    last_name = models.CharField(max_length=50, blank=False)
    type = models.CharField(
        max_length=1,
        choices=USER_TYPE_CHOICES,
        default='s'
    )
    company = models.ForeignKey(
        Company,
        related_name='users',
        on_delete = models.CASCADE,
        null=True,
    )
    profile_pic = models.ImageField(
        upload_to='accounts/profile_pic/',
        default='accounts/profile_pic/profile.png',
    )
    phone = models.CharField(max_length=13, blank=True)

    address_line_1 = models.CharField(max_length=100, blank=True)
    address_line_2 = models.CharField(max_length=100, blank=True)
    city = models.CharField(max_length=50, blank=True)
    state = models.CharField(max_length=50, blank=True)
    country = models.CharField(max_length=50, blank=True)
    pincode = models.CharField(max_length=50, blank=True)


    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return self.email

class EventExtraField(models.Model):
    company = models.ForeignKey(
        Company,
        related_name='extra_event_fields',
        on_delete=models.CASCADE
    )
    name = models.CharField(max_length=100, blank=False)
    type = models.CharField(
        max_length=100,
        choices=FIELD_TYPE_CHOICES,
        default='1'
    )
    dropdown_choices = models.TextField(blank=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name
