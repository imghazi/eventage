from django.contrib.auth.forms import UserCreationForm as DjangoUserCreationForm, AuthenticationForm
from django.forms import ModelForm, inlineformset_factory
from django import forms

from . import models, utils

from datetime import date

FIELD_TYPE_CHOICES = [
    ('1', 'String'),
    ('2', 'Date'),
    ('3', 'Email'),
    ('4', 'File'),
    ('5', 'Dropdown')
]

class UserCreationForm(DjangoUserCreationForm):

    class Meta(DjangoUserCreationForm):
        model = models.User
        fields = (
            'email',
            'first_name',
            'last_name',
            'password1',
            'password2',
        )

class AddUserForm(ModelForm):
    type = forms.ChoiceField(
        choices=[
            ('0', 'Normal'),
            ('1', 'Manager')
        ],
        widget=forms.Select(
            attrs={
                'class': 'form-control'
            }
        )
    )
    class Meta(ModelForm):
        model = models.User
        fields = (
            'email',
            'first_name',
            'last_name',
            'type',
            'phone',
            'profile_pic',
        )
        widgets = {
            'email': forms.EmailInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'first_name': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'last_name': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'phone': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'profile_pic': forms.FileInput(
                attrs={
                    'class': 'form-control'
                }
            )
        }

class CustomAuthenticationForm(AuthenticationForm):
    def confirm_login_allowed(self, user):
        if not user.is_superuser:
            if user.company.subscription_date < date.today():
                raise forms.ValidationError('Your subscription has expired. Please contact your administrator.', code='invalid_login')
            if not user.company.is_active:
                raise forms.ValidationError('Username/Password combination not found!', code='invalid_login')
            elif not user.is_active:
                raise forms.ValidationError('Your account has been deactivated. Please contact your manager/admin.', code='invalid_login')

class SetPasswordForm(DjangoUserCreationForm):
    class Meta(ModelForm):
        model = models.User
        fields = ('password1', 'password2',)

class UserUpdateForm(ModelForm):

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        user_obj = kwargs['instance']
        super().__init__(*args, **kwargs)

        if self.user and user_obj:
            if (user_obj.type == 's') or (
                user_obj.type == '1' and self.user.type == '1') or (

            ):
                self.fields['is_active'] = forms.BooleanField(
                    label='Status:',
                    widget=forms.CheckboxInput(
                        attrs={
                            'data-size': 'normal',
                            'data-on-color': 'primary',
                            'data-off-color': 'default',
                            'data-on-text': 'Active',
                            'data-off-text': 'Inactive',
                        }
                    )
                )
                self.fields['type'] = forms.ChoiceField(
                    choices=[
                        ('0', 'Normal'),
                        ('1', 'Manager'),
                        ('s', 'Supervisor')
                    ],
                    widget = forms.Select(
                        attrs={
                            'class': 'form-control',
                            'readonly': True
                        }
                    )
                )
            else:
                self.fields['is_active'] = forms.BooleanField(
                    label='Status:',
                    widget=forms.CheckboxInput(
                        attrs={
                            'data-size': 'normal',
                            'data-on-color': 'primary',
                            'data-off-color': 'default',
                            'data-on-text': 'Active',
                            'data-off-text': 'Inactive',
                        }
                    )
                )
                self.fields['type'] = forms.ChoiceField(
                    choices=[
                        ('0', 'Normal'),
                        ('1', 'Manager'),
                    ],
                    widget = forms.Select(
                        attrs={
                            'class': 'form-control',
                        }
                    )
                )

    class Meta(ModelForm):
        model = models.User
        fields = (
            'first_name',
            'last_name',
            'type',
            'profile_pic',
            'phone',
            'is_active',
        )
        widgets = {
            'first_name': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'last_name': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'phone': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'profile_pic': forms.FileInput(
                attrs={
                    'class': 'form-control'
                }
            )
        }

class CompanyCreationForm(ModelForm):
    class Meta(ModelForm):
        model = models.Company
        fields = ('name',)
        labels = {
            'name': 'Company Name',
        }

class CompanyUpdateForm(ModelForm):
    class Meta(ModelForm):
        model = models.Company
        fields = (
            'name',
            'currency',
            # 'images_per_row'
            # 'logo',
            # 'address_line_1',
            # 'address_line_2',
            # 'city',
            # 'state',
            # 'country',
            # 'pincode',
        )
        labels = {
            'name': 'Company Name',
        }
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'currency': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'images_per_row': forms.NumberInput(
                attrs={
                    'class': 'form-control'
                }
            )
        }

class EventExtraFieldForm(ModelForm):

    class Meta:
        model = models.EventExtraField
        fields = (
            'name',
            'type',
            'dropdown_choices'
        )
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'type': forms.Select(
                attrs={
                    'class': 'form-control custom-select'
                }
            ),
            'dropdown_choices': forms.Textarea(
                attrs={
                    'class': 'form-control noresize',
                    'rows': 2,
                    'readonly': True
                }
            ),
        }

EventExtraFieldFormset = inlineformset_factory(
    models.Company,
    models.EventExtraField,
    form=EventExtraFieldForm,
    extra=1
)
