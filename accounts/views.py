from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.decorators import method_decorator
from django.template.loader import render_to_string
# from django.core.mail import send_mail
from django.views import View
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView, CreateView, UpdateView
from django.views.generic import TemplateView
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse

import requests

from . import models, forms, tokens, utils, mixins

#creates email message to inject into email template
def get_message(request, user, email_template):
    current_site = get_current_site(request)
    return render_to_string(
        email_template,
        {
            'user':user,
            'domain':current_site.domain,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'token': tokens.account_activation_token.make_token(user)
        }
    )

# creates a company and its supervisor
class SignUpView(TemplateView):
    template_name = 'accounts/signup.html'

    def post(self, request, *args, **kwargs):
        user_form = forms.UserCreationForm(request.POST)
        company_form = forms.CompanyCreationForm(request.POST)
        if all({user_form.is_valid(), company_form.is_valid()}):

            user = user_form.save(commit=False)
            user.is_active = False

            company = company_form.save()

            user.company = company
            user.save()

            mail_subject = 'Activate Your Eventage Account'
            email_template = 'accounts/activate_account_email_body.html'
            to_email = user_form.cleaned_data.get('email')

            utils.send_mail(
                to_email,
                mail_subject,
                get_message(request, user, email_template)
            )

            return HttpResponseRedirect(reverse('accounts:sent_mail_message'))

        return render(request, self.template_name, {
            'user_form':user_form,
            'company_form':company_form
        })

    def get(self, request, *args, **kwargs):

        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse('home'))

        user_form = forms.UserCreationForm()
        company_form = forms.CompanyCreationForm()

        return render(request, self.template_name, {
            'user_form':user_form,
            'company_form':company_form
        })

# activates account - after clicking link sent to email
class ActivateAccount(View):
    def get(self, request, *args, **kwargs):
        try:
            uid = force_text(urlsafe_base64_decode(kwargs['uidb64']))
            user = models.User.objects.get(pk=uid)
            assert(
                tokens.account_activation_token.check_token(
                    user,
                    kwargs['token']
                )
            )
            user.is_active = True
            user.save()
            user.company.is_active = True
            user.company.save()
        except (models.User.DoesNotExist, AssertionError):
            return HttpResponseRedirect(reverse('accounts:link_expired'))
        return HttpResponseRedirect(reverse('accounts:email_verified'))

# @method_decorator(csrf_protect, name='dispatch')
class PasswordReset(FormView):
    template_name = 'accounts/password_reset_form.html'
    form_class = PasswordResetForm
    success_url = reverse_lazy('accounts:password_reset_done')

    def form_valid(self, form):
        email = form.cleaned_data.get('email')
        try:
            user = models.User.objects.get(email=email)
            mail_subject = 'Reset Your Eventage Account Password'
            email_template = 'accounts/password_reset_email_body.html'
            to_email = email
            utils.send_mail(to_email, mail_subject, get_message(self.request, user, email_template))
        except models.User.DoesNotExist:
            form.add_error(None, 'User with this email doesn\'t exist!')
            return super().form_invalid(form)
        return super().form_valid(form)

#resets password - accessed via link sent to email for forgotten password
class PasswordResetConfirm(FormView):
    form_class = forms.SetPasswordForm
    template_name = 'accounts/password_reset_confirm.html'
    success_url = reverse_lazy('accounts:password_reset_complete')

    def get(self, request, *args, **kwargs):
        try:
            uid = force_text(urlsafe_base64_decode(kwargs['uidb64']))
            user = models.User.objects.get(pk=uid)
            assert(
                tokens.account_activation_token.check_token(
                    user,
                    kwargs['token']
                )
            )
        except (models.User.DoesNotExist, AssertionError):
            return HttpResponseRedirect(reverse('accounts:link_expired'))
        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        uid = force_text(urlsafe_base64_decode(self.kwargs['uidb64']))
        user = models.User.objects.get(pk=uid)
        password = form.cleaned_data.get('password1')
        user.set_password(password)
        user.save()
        return super().form_valid(form)

# creates user - created by company manager
class AddUser(
    LoginRequiredMixin,
    mixins.ManagerRequiredMixin,
    FormView
):
    form_class = forms.AddUserForm
    template_name = 'accounts/new_user.html'
    success_url = reverse_lazy('accounts:user_list')

    def form_valid(self, form):
        company = self.request.user.company
        new_user = form.save(commit=False)
        new_user.company = company
        new_user.set_unusable_password()
        new_user.save()
        mail_subject = 'You were added by ' + company.name + ' to eventage.com'
        email_template = 'accounts/set_password_email_body.html'
        to_email = form.cleaned_data.get('email')
        utils.send_mail(to_email, mail_subject, get_message(self.request, new_user, email_template))
        return super().form_valid(form)

# sets password for new user added by a company
class SetPassword(FormView):
    form_class = forms.SetPasswordForm
    template_name = 'accounts/set_password.html'
    success_url = reverse_lazy('accounts:password_set_message')

    def get(self, request, *args, **kwargs):
        try:
            uid = force_text(urlsafe_base64_decode(kwargs['uidb64']))
            user = models.User.objects.get(pk=uid)
            assert(
                tokens.account_activation_token.check_token(
                    user,
                    kwargs['token']
                )
            )
        except (models.User.DoesNotExist, AssertionError):
            return HttpResponseRedirect(
                reverse('accounts:link_expired')
            )
        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        uid = force_text(urlsafe_base64_decode(self.kwargs['uidb64']))
        user = models.User.objects.get(pk=uid)
        password = form.cleaned_data.get('password1')
        user.set_password(password)
        user.save()
        return super().form_valid(form)

#returns list of users
class ActiveUserListView(LoginRequiredMixin, ListView):
    model = models.User
    template_name='accounts/user_list.html'

    def get(self, request, *args, **kwargs):
        if request.user.type == 'n':
            return redirect('error_404')
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        return models.User.objects.filter(
            is_active=True,
            company=self.request.user.company,
        )

# class UserDetailView(LoginRequiredMixin, DetailView):
#     template_name='accounts/user_detail.html'
#     model = models.User
#     context_object_name = 'user_obj'

#updates user details
class UpdateUser(
    LoginRequiredMixin,
    mixins.ManagerRequiredMixin,
    mixins.SameCompanyOnlyMixin,
    UpdateView
):
    form_class = forms.UserUpdateForm
    model = models.User
    template_name = 'accounts/user_update.html'
    success_url = reverse_lazy('accounts:user_list')
    context_object_name = 'user_obj'

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        kwargs['user'] = self.request.user
        return kwargs

class UpdateCompany(
    LoginRequiredMixin,
    mixins.ManagerRequiredMixin,
    DetailView
):
    model = models.Company
    context_object_name = 'company'
    template_name = 'accounts/company_settings.html'

    def post(self, request, *args, **kwargs):
        company = request.user.company
        if 'submit' in request.POST:
            company_form = forms.CompanyUpdateForm(
                request.POST,
                instance=company
            )
            event_extra_formset = forms.EventExtraFieldFormset(
                request.POST,
                instance=company,
                queryset=models.EventExtraField.objects.filter(
                    company=company,
                    is_active=True
                ),
                prefix='event_extra'
            )
            if all({
                company_form.is_valid(),
                event_extra_formset.is_valid()
            }):
                company_form.save()
                event_extra_formset.save()
            return redirect('accounts:company_settings', pk=self.kwargs['pk'])

    def get_context_data(self, *args, **kwargs):
        data = super().get_context_data(*args, **kwargs)
        company = models.Company.objects.get(pk=self.kwargs['pk'])
        data['company_form'] = forms.CompanyUpdateForm(
            instance=company
        )
        data['event_extra_formset'] = forms.EventExtraFieldFormset(
            instance=company,
            queryset=models.EventExtraField.objects.filter(
                company=company,
                is_active=True
            ),
            prefix='event_extra'
        )
        return data
