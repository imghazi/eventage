from django.urls import path
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView

from . import views, forms

app_name='accounts'

urlpatterns = [
    path(
        'signup/',
        views.SignUpView.as_view(),
        name='signup',
    ),
    path(
        'login/',
        auth_views.LoginView.as_view(
            template_name='accounts/login.html',
            authentication_form=forms.CustomAuthenticationForm
        ),
        name='login',
    ),
    path(
        'activate/<uidb64>/<token>',
        views.ActivateAccount.as_view(),
        name='activate',
    ),
    path(
        'logout',
        auth_views.LogoutView.as_view(),
        name='logout'
    ),
    path('password_reset/',
        views.PasswordReset.as_view(),
        name='password_reset',
    ),
    path('password_reset/done/',
        auth_views.PasswordResetDoneView.as_view(
            template_name='accounts/password_reset_done.html'
        ),
        name='password_reset_done'
    ),
    path('reset/<uidb64>/<token>/',
        views.PasswordResetConfirm.as_view(),
        name='password_reset_confirm'
    ),
    path(
        'reset/done/',
        auth_views.PasswordResetCompleteView.as_view(
            template_name='accounts/password_reset_complete.html'
        ),
        name='password_reset_complete'
    ),
    path('change_password/',
        auth_views.PasswordChangeView.as_view(
            template_name='accounts/change_password.html'
        ),
        name='change_password'
    ),
    path('password_change/done/',
        auth_views.PasswordChangeView.as_view(
            template_name='accounts/password_change_done.html'
        ),
    ),
    path('set_password/<uidb64>/<token>/',
        views.SetPassword.as_view(),
        name='set_password'
    ),
    path('new_user/',
        views.AddUser.as_view(),
        name='new_user'
    ),
    path('user_list',
        views.ActiveUserListView.as_view(),
        name='user_list'
    ),
    # path('user_detail/<pk>/',
    #     views.UserDetailView.as_view(),
    #     name='user_detail'
    # ),
    path('user_update/<pk>/',
        views.UpdateUser.as_view(),
        name='user_update'
    ),
    path('sent_mail_message',
        TemplateView.as_view(
            template_name='accounts/sent_mail_message.html',
        ),
        name='sent_mail_message',
    ),
    path('link_expired',
        TemplateView.as_view(
            template_name='accounts/link_expired.html',
        ),
        name='link_expired'
    ),
    path('email_verified',
        TemplateView.as_view(
            template_name='accounts/email_verified.html',
        ),
        name='email_verified'
    ),
    path('password_set_message',
        TemplateView.as_view(
            template_name='accounts/password_set_message.html'
        ),
        name='password_set_message',
    ),
    path('company/<int:pk>/settings/',
        views.UpdateCompany.as_view(),
        name='company_settings',
    )
]
