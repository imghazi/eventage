from django import forms
from django.conf import settings

import requests
import os

from . import models

LOGO_PATH = os.path.join(settings.BASE_DIR, 'static') + '/assets/images/logos/logo.png'

def send_mail(to_email, mail_subject, message):
    # print('api calling')
    return requests.post(
        "https://api.mailgun.net/v3/mailgun.agentsdash.com/messages",
        auth=("api", "25e7ad288f8f6398e043535053291f12-e470a504-8c9e954a"),
        files=[('inline', open(LOGO_PATH, 'rb'))],
        data={
            "from": "eventage.com <noreply@eventage.com>",
            "to": [to_email,],
            "subject": mail_subject,
            "html": message,
        }
    )
