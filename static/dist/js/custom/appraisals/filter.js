function performAction(object){
  console.log($(object).val());
  let pk = $(object).data('investorPk')
  if($(object).val()=='1'){
    $('#create_investor_pk').val(pk);
    $('#createInvestorModal').modal('show');
  }
  else if($(object).val()=='2'){
    $('#temp_investor_pk').val(pk);
    $('#assignInvestorModel').modal('show');
  }
  else if($(object).val()=='3'){
    console.log('working');
    $('#delete_investor_pk').val(pk);
    $('#deleteTempInvestorModal').modal('show');
  }
}

$(document).ready(function(){
  $('.datatable-select-inputs').DataTable();
})
