$(document).ready(function(){
  $('.datatable-select-inputs').DataTable({
      initComplete: function() {
          this.api().columns([0, 2,]).every(function() {
              console.log(this)
              var column = this;
              var select = $('<select class="form-control custom-select"><option value="">All</option></select>')
                  .appendTo($(column.footer()).empty())
                  .on('change', function() {
                      var val = $.fn.dataTable.util.escapeRegex(
                          $(this).val()
                      );

                      column
                          .search(val ? '^' + val + '$' : '', true, false)
                          .draw();
                  });

              column.data().unique().sort().each(function(d, j) {
                  select.append('<option value="' + d + '">' + d + '</option>');
              });
          });
      }
  });
})
