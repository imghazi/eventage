from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from django.conf import settings
from django.conf.urls.static import static


from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path(
        '',
        views.HomeView.as_view(),
        name='home'
    ),
    path(
        'accounts/',
        include(
            'accounts.urls',
            namespace='accounts'
        )
    ),
    path(
        'accounts/',
        include(
            'django.contrib.auth.urls'
        )
    ),
    path(
        'events/',
        include(
            'pageGenerator.urls',
            namespace='pageGenerator'
        )
    ),
    path('error-404/',
        TemplateView.as_view(template_name='error_404.html'),
        name='error_404',
    ),
    path('error-403/',
        TemplateView.as_view(template_name='error_403.html'),
        name='error_403',
    ),
    path('thankyou/',
        TemplateView.as_view(template_name='thankyou.html'),
        name='thankyou',
    ),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
