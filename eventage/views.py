from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import redirect
from django.http import JsonResponse

from accounts import models

from datetime import date, timedelta
from decimal import *

class HomeView(LoginRequiredMixin, TemplateView):
    template_name='index.html'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        company = self.request.user.company
        data['user_list'] = models.User.objects.filter(company=company)
        return data
